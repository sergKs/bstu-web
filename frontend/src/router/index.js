import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter);

const routes = [
	{
		path: '/',
		name: 'home',
		component: Home
	},
	{
		path: '/about',
		name: 'about',
		component: () => import('../views/About.vue')
	},
	{
		path: '/news',
		name: 'news',
		component: () => import('../views/News.vue'),
		meta: {
			layout: 'news'
		}
	},
	{
		path: '/news/:id',
		name: 'article',
		component: () => import('../views/Article.vue')
	},
	{
		path: '/login',
		name: 'login',
		component: () => import('../views/Login.vue'),
		meta: {
			layout: 'login'
		}
	},
	{
		path: '/cart',
		name: 'cart',
		component: () => import('../views/Cart.vue')
	}
];

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes
});

export default router
