<?php

namespace app\controllers;

use app\models\Article;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class NewsController extends Controller
{
	/**
	 * Список всех новостей.
	 *
	 * @return string
	 */
	public function actionIndex()
	{
		$models = Article::find()
			->with(['category', 'images'])
			->orderBy(['createdAt' => SORT_DESC])
			->all();

		return $this->render('index', [
			'models' => $models
		]);
	}

	/**
	 * Отображает статью по ID.
	 *
	 * @param $id
	 * @return string
	 * @throws NotFoundHttpException
	 */
	public function actionView($id)
	{
		$model = $this->findModel($id);

		return $this->render('view', [
			'model' => $model
		]);
	}

	/**
	 * Добавляет новую статью.
	 *
	 * @return string|\yii\web\Response
	 */
	public function actionCreate()
	{
		$model = new Article();

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['index']);
		}

		return $this->render('create', [
			'model' => $model
		]);
	}

	/**
	 * Поиск модели по ID.
	 *
	 * @param $id
	 * @return Article
	 * @throws NotFoundHttpException
	 */
	protected function findModel($id)
	{
		$model = Article::findOne($id);

		if ($model === null) {
			throw new NotFoundHttpException('Статья не найдена.');
		}

		return $model;
	}
}