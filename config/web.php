<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$testDb = require __DIR__ . '/test_db.php';

$config = [
    'id' => 'basic',
	'language' => 'ru',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '12eddasasdd',
	        'parsers' => [
		        'application/json' => 'yii\web\JsonParser',
		        'multipart/form-data' => 'yii\web\MultipartFormDataParser'
	        ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
	    'authManager' => [
		    'class' => 'yii\rbac\PhpManager',
		    'defaultRoles' => ['admin', 'user'],
	    ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => (YII_ENV === 'test' ? $testDb : $db),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
	        //'enableStrictParsing' => true,
            'rules' => [
            	'news/<id:\d+>' => 'news/view',
	            'articles' => 'news/index',

	            [
	            	'class' => 'yii\rest\UrlRule',
		            'controller' => ['api/news']
	            ],

	            //'api/<controller>/<id:\d+><action>' => 'api/<controller>/<action>',
	            'api/<controller>/<action>' => 'api/<controller>/<action>'
            ],
        ]
    ],
    'params' => $params,
	'modules' => [
		'api' => [
			'class' => 'app\modules\api\ApiModule'
		]
	]
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];
}

return $config;
