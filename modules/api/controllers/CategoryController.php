<?php

namespace app\modules\api\controllers;

use app\models\Category;
use yii\rest\Controller;

class CategoryController extends Controller
{
	public function verbs()
	{
		return [
			'last' => ['GET'],
			'add' => ['POST']
		];
	}

	public function actionLast()
	{
		$models = Category::find()
			->orderBy(['createdAt' => SORT_DESC])
			->limit(10)
			->all();

		return $models;
	}

	public function actionAdd()
	{

	}
}
