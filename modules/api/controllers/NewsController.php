<?php

namespace app\modules\api\controllers;

use app\models\Article;

class NewsController extends BaseActiveController
{
	public $modelClass = 'app\models\Article';
}