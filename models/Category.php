<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $createdAt Дата создания
 * @property string $updatedAt Дата изменения
 *
 * @property Article[] $news
 */
class Category extends BaseModel
{
	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'categories';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['name'], 'required'],
			[['createdAt', 'updatedAt'], 'safe'],
			[['name'], 'string', 'max' => 128],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Наименование',
			'createdAt' => 'Дата создания',
			'updatedAt' => 'Дата изменения',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getNews()
	{
		return $this->hasMany(Article::className(), ['categoryId' => 'id']);
	}

	/**
	 * Возвращает список категорий.
	 *
	 * @return array
	 */
	public static function getAll()
	{
		$models = static::find()->all();
		return ArrayHelper::map($models, 'id', 'name');
	}
}
