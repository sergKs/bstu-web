<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m191116_104826_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
	        'email' => $this->string(128)->notNull()->comment('Email'),
	        'password' => $this->string(128)->notNull()->comment('Пароль'),
	        'accessToken' => $this->string(128)->notNull(),
	        'authKey' => $this->string(128)->notNull(),
	        'createdAt' => $this->dateTime()->notNull()->comment('Дата создания'),
	        'updatedAt' => $this->dateTime()->comment('Дата изменения')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }
}
