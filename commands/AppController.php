<?php

namespace app\commands;

use app\components\UserRule;
use app\models\User;
use Yii;
use yii\console\Controller;

class AppController extends Controller
{
	public function actionInit()
	{
		$user = new User();
		$user->email = 'admin@admin.ru';
		$user->password = '123456';
		$user->save();
	}

	/**
	 * Выполняет настройку приложения
	 */
	public function actionInstall()
	{
		$auth = Yii::$app->authManager;

		$rule = new UserRule();
		$auth->add($rule);

		$user = $auth->createRole('user');
		$user->ruleName = $rule->name;
		$user->description = 'Пользователь';
		$auth->add($user);

		$admin = $auth->createRole('admin');
		$admin->ruleName = $rule->name;
		$admin->description = 'Администратор';
		$auth->add($admin);
		$auth->addChild($admin, $user);
	}
}